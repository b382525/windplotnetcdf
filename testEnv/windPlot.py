### Winda - Wind Plot Software ###
### Developed by William Djalal ###
### 2023 - 2024 ###

import os
import sys
import re
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.animation
import time
import random
import numpy as np
import xarray as xr
import netCDF4 as nc
from datetime import datetime

stringText = ''
testEvent = "201500019" #gute testnummer: 201500019
frevaid = "928616"
eventLength = [0] * 1000
currEventSpace = [0] * 1000
listEvents = [0] * 100
selectedPlot = [0, 0]
lonVals = [0] * 100
latVals = [0] * 100
dateVals = [0] * 100
miniTestCounter = 0
userInputStageDone = False #If switchted to True later, only selected Event will be shown
selectedEventNum = 0
specifiedDate = 0

countSpecificEvent = 0
lonValsFiltered = [0] * 1000
lonValsFilteredList = [0] * 1000
latValsFiltered = [0] * 1000
latValsFilteredList = [0] * 1000

#Open File
txtfilepath = "/work/bm1159/XCES/xces-work/b382525/evaluation_system/output/wtrack/cmip6_multimodel/" + frevaid + "/wtrack_out/cmip6/bcc-csm2-mr/ssp585/WIND_EVENT/r1i1p1f1/event_tracks_2015_bcc-csm2-mr_ssp585_r1i1p1f1_2015.dat"
with open(txtfilepath, 'r') as f:
    lines = f.readlines()

#Remove first 2 Lines
del lines[0]
del lines[0]

#Replace Spaces
lines = [line.replace(' ', ',') for line in lines]

for x in lines:
    stringText += x

#Split String
stringTextSplit = re.split(r',|\n', stringText)

#Filter String
def removeEmptyStrings(element):
    if element == '':
        return False
    
    return True
def removeZero(element):
    if element == 0:
        return False
    
    return True

filteredTextTemp = filter(removeEmptyStrings, stringTextSplit)
filteredText = list(filteredTextTemp)

def searchingMultipleEvents():
    print("Searching Multiple Events...")
    global countSpecificEvent
    global specifiedDate

    #Get Place & Length of Events
    for i in range(len(filteredText)):
        if filteredText[i] == "Event:":
            numOfDatesInEvent = int(filteredText[i+5]) #Number of dates
            #Check if selected Event includes entered date
            for x in range(numOfDatesInEvent):
                selDate = filteredText[i + 12 + 17 * x][:-2]
                #Found One Event
                if (selDate == specifiedDate):
                    currEventSpace[countSpecificEvent] = i+1 #Holds current place of event number
                    eventLength[countSpecificEvent] = int(filteredText[i+5]) #Number of plot points in event
                    listEvents[countSpecificEvent] = filteredText[i+1] #Save Event number
                    countSpecificEvent += 1
                    print("Found Event: " + filteredText[i+1] + " with " + str(numOfDatesInEvent) + " plot points!")
                    break
        #End of File
        if i == len(filteredText) - 1:
            break

    #Get LON,LAT from MULTIPLE Events
    allLonsOfAllEvents = []
    allLatsOfAllEvents = []
    for i in range(countSpecificEvent):
        allLonsOfSpecEvent = [0] * 50
        allLatsOfSpecEvent = [0] * 50
        for j in range(eventLength[i]):
            allLonsOfSpecEvent[j] = filteredText[currEventSpace[i] + 15 + (17 * j)]
            allLatsOfSpecEvent[j] = filteredText[currEventSpace[i] + 16 + (17 * j)]
        allLonsOfAllEvents.append(allLonsOfSpecEvent)
        allLatsOfAllEvents.append(allLatsOfSpecEvent)

    #INCLUDE LATER#
    #Get dates from specific Event
    #tickIndex2 = currEventSpace + 11
    #if filteredText[currEventSpace] == testEvent:
        #for x in range(eventLength):
            #dateVals[x] = filteredText[tickIndex2]
            #tickIndex2 += 17
    #dateValsTrimmed = np.trim_zeros(dateVals)

    #Filter empty array elements
    for i in range(countSpecificEvent):
        lonValsFiltered[i] = filter(removeZero, allLonsOfAllEvents[i])
        lonValsFilteredList[i] = list(lonValsFiltered[i])

    for i in range(countSpecificEvent):
        latValsFiltered[i] = filter(removeZero, allLatsOfAllEvents[i])
        latValsFilteredList[i] = list(latValsFiltered[i])


    #Convert to float
    for i in range(countSpecificEvent):
        for j in range(0, len(lonValsFilteredList[i])):
            lonValsFilteredList[i][j] = float(lonValsFilteredList[i][j])

        for j in range(0, len(latValsFilteredList[i])):
            latValsFilteredList[i][j] = float(latValsFilteredList[i][j])

def searchingSpecificEvent():
    print("Searching Specific Event...")
    global countSpecificEvent
    countSpecificEvent = 0
    lonValsFiltered = [0] * 1000
    lonValsFilteredList = [0] * 1000
    latValsFiltered = [0] * 1000
    latValsFilteredList = [0] * 1000

    #Get Place & Length of Events
    for i in range(len(filteredText)):
        if filteredText[i] == "Event:":
            if filteredText[i+1] == selectedEventNum: #If Event is selected Event
                print("Found Event: " + filteredText[i+1])
                numOfDatesInEvent = int(filteredText[i+5]) #Number of dates
                #Check if selected Event includes entered date
                for x in range(numOfDatesInEvent):
                    currEventSpace[countSpecificEvent] = i+1 #Holds current place of event number
                    eventLength[countSpecificEvent] = int(filteredText[i+5]) #Number of plot points in event
                    listEvents[countSpecificEvent] = filteredText[i+1] #Save Event number
                    countSpecificEvent += 1
                    print("Found Specific Event: " + filteredText[i+1] + " with " + str(numOfDatesInEvent) + " plot points!")
                    break
        #End of File
        if i == len(filteredText) - 1:
            break

    #Get LON,LAT from MULTIPLE Events
    allLonsOfAllEvents = []
    allLatsOfAllEvents = []
    allLonsOfSpecEvent = [0] * 50
    allLatsOfSpecEvent = [0] * 50
    for j in range(eventLength[0]):
        allLonsOfSpecEvent[j] = filteredText[currEventSpace[0] + 15 + (17 * j)]
        allLatsOfSpecEvent[j] = filteredText[currEventSpace[0] + 16 + (17 * j)]
    allLonsOfAllEvents.append(allLonsOfSpecEvent)
    allLatsOfAllEvents.append(allLatsOfSpecEvent)

    #INCLUDE LATER#
    #Get dates from specific Event
    #tickIndex2 = currEventSpace + 11
    #if filteredText[currEventSpace] == testEvent:
        #for x in range(eventLength):
            #dateVals[x] = filteredText[tickIndex2]
            #tickIndex2 += 17
    #dateValsTrimmed = np.trim_zeros(dateVals)

    #Filter empty array elements
    for i in range(countSpecificEvent):
        lonValsFiltered[i] = filter(removeZero, allLonsOfAllEvents[i])
        lonValsFilteredList[i] = list(lonValsFiltered[i])

    for i in range(countSpecificEvent):
        latValsFiltered[i] = filter(removeZero, allLatsOfAllEvents[i])
        latValsFilteredList[i] = list(latValsFiltered[i])


    #Convert to float
    for i in range(countSpecificEvent):
        for j in range(0, len(lonValsFilteredList[i])):
            lonValsFilteredList[i][j] = float(lonValsFilteredList[i][j])

        for j in range(0, len(latValsFilteredList[i])):
            latValsFilteredList[i][j] = float(latValsFilteredList[i][j])


#Plot Result
def plotting():
    fig = plt.figure(figsize=(12,9))
    currMarkerPoint = 0
    global userInputStageDone
    global selectedEventNum
    global countSpecificEvent
    global specifiedDate

    #Ask User for Date
    if (userInputStageDone == False):
        specifiedDate = input("Please enter a date (YYYYMMDD):")
        searchingMultipleEvents()

    # convert lon to correct values
    correctedLonValsFilteredList = lonValsFilteredList
    for i in range(countSpecificEvent):
        for j in range(len(correctedLonValsFilteredList[i])):
            correctedLonValsFilteredList[i][j] = (correctedLonValsFilteredList[i][j] + 180) % 360 - 180

    #Animate
    for k in range(1000):
        #plt.clf()

        # drawing the coastline
        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.coastlines()

        #Draw wind map
        #path to .nc file
        ncfilepath = "/work/bm1159/XCES/xces-work/b382525/evaluation_system/output/wtrack/cmip6_multimodel/" + frevaid + "/wtrack_out/cmip6/bcc-csm2-mr/ssp585/CACHE/ssp585/r1i1p1f1/wind_2015-2016.nc"
        ds = xr.open_dataset(ncfilepath)

        #path to .nc percentile file
        ncPercentilefilepath = "/work/bm1159/XCES/xces-work/b382525/evaluation_system/output/wtrack/cmip6_multimodel/" + frevaid + "/wtrack_out/cmip6/bcc-csm2-mr/ssp585/PERCENTILE/perc_file_98_bcc-csm2-mr_historical_wind_0_360_-90_90_1980_2000.nc"
        ds2 = xr.open_dataset(ncPercentilefilepath)

        windData = ds.variables['wind'][:]
        windLats = ds.variables['lat'][:]
        windLons = ds.variables['lon'][:]
        windTimes = ds.variables['time'][:]

        windData2 = ds2.variables['wind'][:]
        windLats2 = ds2.variables['lat'][:]
        windLons2 = ds2.variables['lon'][:]

        #correcting and sorting lon values for correct plotting
        windLonsLength = len(windLons)
        for x in range(windLonsLength):
            windLons.values[x] = (windLons.values[x] + 180) % 360 - 180
        
        windLons.values.sort()

        windLons2Length = len(windLons2)
        for x in range(windLons2Length):
            windLons2.values[x] = (windLons2.values[x] + 180) % 360 - 180
        
        windLons2.values.sort()

        # plotting the wind map
        lon, lat = np.meshgrid(windLons, windLats)

        lon2, lat2 = np.meshgrid(windLons2, windLats2)


        # INCLUDE BACK LATER#
        # transforming specific date to useable time for sel function
        #currDate = dateValsTrimmed[currMarkerPoint]
        #currDateTweaked = currDate[:4] + "-" + currDate[4:6] + "-" + currDate[6:8] + "T" + currDate[8:10] + ":00:00"

        #c_scheme = ax.pcolor(lon, lat, np.squeeze(ds.wind.sel(time=currDateTweaked)), cmap = 'jet', vmin=0, vmax=50)
        #cb = fig.colorbar(c_scheme, shrink=0.5)
        #cb.ax.set_title('Wind Speed [m/s]')
        #cb.ax.autoscale(False)
        
        #calculating cont
        windDataLength = len(windData)
        percentileList = windData2.values[0]

        latNum = len(windLats)
        latMax = len(windLats2)
        for y in range(windDataLength):
            ds.wind.values[y] = ds.wind.values[y] - np.squeeze(ds2.wind[:,:,latMax-latNum:latMax,:].values)
        
        #np.clip(ds.wind.values, 0, 1000)
        #print(ds.wind)

        plt.rcParams['contour.negative_linestyle'] = 'solid'
        #INCLUDE BACK LATER#
        #ax.contour(lon, lat, np.squeeze(ds.wind.sel(time=currDateTweaked)), colors='red', linewidths=3)

        #INCLUDE BACK LATER#
        # plotting the map
        #ax.set_extent([-20, 40, 40, 65]) # boundaries of map
        ax.set_extent([-180, 180, -90, 90]) # boundaries of map
        #ax.scatter(lonValsFilteredList, latValsFilteredList) # plot lot lan vals

        # draw line
        listEventsCut = np.trim_zeros(listEvents, trim='fb') #Trim Zeros
        for i in range(countSpecificEvent):
            plotLonPoints = correctedLonValsFilteredList[i]
            plotLatPoints = latValsFilteredList[i]
            plt.plot(plotLonPoints, plotLatPoints, color='red', linewidth=2, marker='o')
            # draw event annotations
            plt.annotate(listEventsCut[i], xy=(plotLonPoints[0], plotLatPoints[0]), fontsize=16, arrowprops={'facecolor': 'red', 'shrink': 0.02}, xytext=(plotLonPoints[0], plotLatPoints[0]-12))

        #INCLUDE BACK LATER#
        #plt.annotate('Event: ' + testEvent + '   ' + 'Date: ' + currDateTweaked + '           ' + 'Center Lon: ' + str(round(correctedLonValsFilteredList[currMarkerPoint], 2)) + ' ' + 'Center Lat: ' + str(latValsFilteredList[currMarkerPoint]), xy=(0, 1.2), fontsize=16 , xycoords='axes fraction')
        plt.annotate('Freva ID: ' + frevaid, xy=(0, 1.1), fontsize=16 , xycoords='axes fraction')

        selectedPlot[0] = correctedLonValsFilteredList[currMarkerPoint]
        selectedPlot[1] = latValsFilteredList[currMarkerPoint]
        currMarkerPoint += 1

        if (currMarkerPoint > len(correctedLonValsFilteredList)-1):
            currMarkerPoint = 0

        # draw green marker
        #INCLUDE BACK LATER#
        #ax.scatter(selectedPlot[0], selectedPlot[1], s=500, color='g', marker='o', alpha=0.8)

        # save fig
        plt.savefig("image.png")


        #plt.waitforbuttonpress() # continue with mouseclick
        if (userInputStageDone):
            input('Input for next step...') # continue with console input
            plt.cla()
            plt.clf()
        else:
            #Ask User to select specific Event
            selectedEventNum = input("Please select and enter an event number:")
            userInputStageDone = True
            searchingSpecificEvent()
            plt.cla()
            plt.clf()

    plt.show()
   
plotting()